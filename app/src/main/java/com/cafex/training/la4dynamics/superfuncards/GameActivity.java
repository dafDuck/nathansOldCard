package com.cafex.training.la4dynamics.superfuncards;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.opengl.Visibility;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.VideoView;
import java.util.Collections;
import java.util.Stack;
import android.widget.EditText;

public class GameActivity extends AppCompatActivity {

    // declare the state that our game needs!
    private int score;
    private Stack<Integer> cards;
    private int currentCard;
    public int highScore;
    public boolean isGameOver;
    public TextView scoreLabel;
    public TextView highScoreLabel;

    private void initGame()
    {
        this.score = 0;
        this.highScore =0;
        this.cards = new Stack<>();
        isGameOver = false;

        // fill the cards array
        for(int i=1; i < 14; i++)
            cards.add(i);

        // shuffle the deck!
        Collections.shuffle(cards);

        // pop the first card, and set the value of the text field
        this.setCurrentCard(cards.pop());
    }

    private void setCurrentCard(int value)
    {
        this.currentCard = value;

        // set the display!
        TextView textView = (TextView) findViewById(R.id.details);
        textView.setText("Card value: " + this.currentCard);

       // final TextView highScore = (TextView) this.findViewById(R.id.highScore);
       // final TextView score = (TextView) this.findViewById(R.id.score);

        this.setTitle("Score: " + this.score);
    }

    // Place to prepare the UI and the default state
    // of this activity!!!
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.activity_game);

        Button tryAgain = (Button) this.findViewById(R.id.tryAgainButton);

        tryAgain.setVisibility(View.INVISIBLE);


        TextView highScoreLabel = (TextView) this.findViewById(R.id.highScore);
        TextView scoreLabel = (TextView) this.findViewById(R.id.score);

        highScoreLabel.setVisibility(View.INVISIBLE);


        int scoreValue = this.getIntent().getIntExtra("score", 0);
        scoreLabel.setText("score: " + scoreValue);

        SharedPreferences settings = getSharedPreferences("GAME_DATA", Context.MODE_PRIVATE);
        int highScoreValue = settings.getInt("HIGH_SCORE", 0);


        if (scoreValue > highScoreValue) {
            highScoreLabel.setText("High score: " + scoreValue);

            //save
            SharedPreferences.Editor editor = settings.edit();
            editor.putInt("HIGH_SCORE", scoreValue);
            editor.commit();
        } else {
            highScoreLabel.setText("High Score: " + highScoreValue);
        }

        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switch (view.getId()) {

                    case R.id.tryAgainButton:
                            gameOverer();
                        break;

                    case (R.id.lower):

                        if (cards.isEmpty())
                            gameOver();
                        else {
                            int nextCard = cards.pop();
                            boolean isCorrect = nextCard > currentCard && view.getId() == R.id.higher ||
                                    nextCard < currentCard && view.getId() == R.id.lower;

                            if (isCorrect) {
                                score++;
                                setCurrentCard(nextCard);
                            } else {
                                isGameOver = true;
                                gameOverer();
                            }
                        }
                        break;

                    case (R.id.higher):

                        if (cards.isEmpty())
                            gameOverer();
                        else {
                            int nextCard = cards.pop();
                            boolean isCorrect = nextCard > currentCard && view.getId() == R.id.higher ||
                                    nextCard < currentCard && view.getId() == R.id.lower;

                            if (isCorrect) {
                                score++;
                                setCurrentCard(nextCard);
                            } else {
                                isGameOver = true;
                                gameOverer();
                            }
                        }
                        break;

                        default:
                            break;

                }
            }
        };

        Button downButton = (Button) findViewById(R.id.lower);
        Button upButton = (Button) findViewById(R.id.higher);

        downButton.setOnClickListener(listener);
        upButton.setOnClickListener(listener);
        tryAgain.setOnClickListener(listener);

    }

    @Override
    protected void onResume() {
        super.onResume();
        this.initGame();
    }

    // private method - only called from this class
    // void - method returns nothing
    // gameOver - method name
    private void gameOver() {

        // The GameActivity has done everything it needed to!
        // Game is over - so we will push the ResultActivity
        // onto the view stack.

        Intent intent = new Intent(getApplication(), ResultActivity.class);

        // pass the score to the ResultActivity - put it inside the Intent!!
        intent.putExtra("score", this.score);

        // now fire off the intent!
        startActivity(intent);
    }


    private void gameOverer(){

        TextView highScoreLabel = (TextView) this.findViewById(R.id.highScore);
        Button tryAgain = (Button) this.findViewById(R.id.tryAgainButton);

        if(isGameOver) {
            highScoreLabel.setVisibility(View.VISIBLE);
            tryAgain.setVisibility(View.INVISIBLE);

        }
        else
        {
            highScoreLabel.setVisibility(View.INVISIBLE);
        }
    }
    public void tryAgain(View view) {
        startActivity(new Intent(getApplicationContext(), GameActivity.class));
    }
}