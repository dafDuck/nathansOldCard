package com.cafex.training.la4dynamics.superfuncards;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.VideoView;


public class ResultActivity extends AppCompatActivity {
    Button clk;
    VideoView videoV;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);


        clk = (Button) findViewById(R.id.button);
        videoV = (VideoView)findViewById(R.id.videoView);

        this.videoPlay(videoV);

        TextView score = (TextView) this.findViewById(R.id.score);
        TextView highScore = (TextView) this.findViewById(R.id.highScore);

        int scoreValue = this.getIntent().getIntExtra("score", -1);
        score.setText(scoreValue + "");

        SharedPreferences settings = getSharedPreferences("GAME_DATA", Context.MODE_PRIVATE);
        int highScoreValue = settings.getInt("HIGH_SCORE", 0);

        if(scoreValue > highScoreValue)
        {
            highScore.setText("High score: " + scoreValue);

            //save
            SharedPreferences.Editor editor = settings.edit();
            editor.putInt("HIGH_SCORE", scoreValue);
            editor.commit();
        }
        else
        {
            highScore.setText("High Score: " + highScoreValue);
        }
    }

    public void tryAgain(View view)
    {
        startActivity(new Intent(getApplicationContext(), GameActivity.class));
    }

    public void videoPlay(View v)
    {
        String videoPath = "android.resource://com.cafex.training.la4dynamics.superfuncards/"+R.raw.profamity;
        Uri uri = Uri.parse(videoPath);
        videoV.setVideoURI(uri);
        videoV.start();
    }
}